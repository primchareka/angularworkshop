import { Component, OnInit } from '@angular/core';
import { categories } from './catergories';
import { SwapiService } from './service/swapi.service';
import { People } from './model/people';
import { Planet } from './model/planet';
import { Specie } from './model/specie';
import { Starship } from './model/starship';
import { Vehicle } from './model/vehicle';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'The Swap Item Database';
  categories = categories
  category: string
  id: number
  data: any
  isFound: boolean
  constructor(private swapi: SwapiService) {
    this.isFound = true
   }

  ngOnInit() {
  }

  getData() {
    let request
    let errorhandler = () =>{this.isFound=false}
    switch (this.category) {
      case "people":
        request = this.swapi.get<People>('people', this.id, errorhandler);
        break;
      case "planets":
        request =this.swapi.get<Planet>('planets', this.id,errorhandler)
        break;
      case "species":
        request =this.swapi.get<Specie>('species', this.id,errorhandler)
        break;
      case "starships":
        request =this.swapi.get<Starship>('starships', this.id,errorhandler)
        break;
      case "vehicles":
        request =this.swapi.get<Vehicle>('vehicles', this.id,errorhandler)
        break;
    }
    if(request != null){
      request.subscribe((data) => {
        this.isFound = true
        this.data = data;
        console.log(data)
      })
    }
  }
}
