import { Component, OnInit, Input } from '@angular/core';
import { People } from '../model/people';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {

  @Input() people: People 
  constructor() { }

  ngOnInit() {
  }

}
