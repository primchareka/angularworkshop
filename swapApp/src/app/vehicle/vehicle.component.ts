import { Component, OnInit } from '@angular/core';
import {Input} from '@angular/core'
import { Vehicle } from '../model/vehicle';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {

  @Input() vehicle: Vehicle
  constructor() { }

  ngOnInit() {
  }

}
