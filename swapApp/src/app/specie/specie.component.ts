import { Component, OnInit, Input } from '@angular/core';
import { Specie } from '../model/specie';

@Component({
  selector: 'app-specie',
  templateUrl: './specie.component.html',
  styleUrls: ['./specie.component.css']
})
export class SpecieComponent implements OnInit {

  @Input() specie: Specie
  constructor() { }

  ngOnInit() {
  }

}
